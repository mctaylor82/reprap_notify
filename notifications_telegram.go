package main

import (
	"log"

	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
	"time"
)

func sendTelegram(messageTitle string, messageText string) {

	client := http.Client{
		Timeout: time.Duration(10 * time.Second),
	}
	url := `https://api.telegram.org/bot` + viper.GetString(`notifications.telegram.token`) + `/sendMessage?chat_id=` + viper.GetString(`notifications.telegram.chatid`) + `&text=` + messageText

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Printf("Unable to create a new http request: %v", err)
	}

	res, err := client.Do(req)
	if err != nil {
		log.Printf("Error while getting data: %v", err)
	}

	if res.StatusCode != http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(res.Body)
		if err != nil {
			log.Printf("Error: %v", err)
		}

		bodyString := string(bodyBytes)
		log.Printf(`Error while sending Telegram message: %v`, bodyString)
	}

}
