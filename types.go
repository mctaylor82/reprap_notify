package main

type rrFileinfoType struct {
	Err              int       `json:"err"`
	Size             int       `json:"size"`
	LastModified     string    `json:"lastModified"`
	Height           float64   `json:"height"`
	FirstLayerHeight float64   `json:"firstLayerHeight"`
	LayerHeight      float64   `json:"layerHeight"`
	PrintTime        int       `json:"printTime"`
	Filament         []float64 `json:"filament"`
	PrintDuration    int       `json:"printDuration"`
	FileName         string    `json:"fileName"`
	GeneratedBy      string    `json:"generatedBy"`
}

type rrStatusType struct {
	// Status 1 Response
	Status string `json:"status"`
	Coords struct {
		AxesHomed []int     `json:"axesHomed"`
		Wpl       int       `json:"wpl"`
		Xyz       []float64 `json:"xyz"`
		Machine   []float64 `json:"machine"`
		Extr      []float64 `json:"extr"`
	} `json:"coords"`
	Speeds struct {
		Requested float64 `json:"requested"`
		Top       float64 `json:"top"`
	} `json:"speeds"`
	CurrentTool int `json:"currentTool"`
	Params      struct {
		AtxPower int `json:"atxPower"`
		// Commenting out FanPercent which is not used. This sometimes returns array, or int...
		// FanPercent  []int     `json:"fanPercent"`
		SpeedFactor float64   `json:"speedFactor"`
		ExtrFactors []float64 `json:"extrFactors"`
		Babystep    float64   `json:"babystep"`
	} `json:"params"`
	Seq     int `json:"seq"`
	Sensors struct {
		ProbeValue int `json:"probeValue"`
		// Commenting out FanRPM which is not used. This sometimes returns array, or int...
		// FanRPM     []int `json:"fanRPM"`
	} `json:"sensors"`
	Temps struct {
		Bed struct {
			Current float64 `json:"current"`
			Active  float64 `json:"active"`
			Standby float64 `json:"standby"`
			State   int     `json:"state"`
			Heater  int     `json:"heater"`
		} `json:"bed"`
		Current []float64 `json:"current"`
		State   []int     `json:"state"`
		Tools   struct {
			Active  [][]float64 `json:"active"`
			Standby [][]float64 `json:"standby"`
		} `json:"tools"`
		Extra []struct {
			Name string  `json:"name"`
			Temp float64 `json:"temp"`
		} `json:"extra"`
	} `json:"temps"`
	Time float64 `json:"time"`
	// Status 3 Response
	CurrentLayer       int       `json:"currentLayer"`
	CurrentLayerTime   float64   `json:"currentLayerTime"`
	ExtrRaw            []float64 `json:"extrRaw"`
	FractionPrinted    float64   `json:"fractionPrinted"`
	FirstLayerDuration float64   `json:"firstLayerDuration"`
	FirstLayerHeight   float64   `json:"firstLayerHeight"`
	PrintDuration      float64   `json:"printDuration"`
	WarmUpDuration     float64   `json:"warmUpDuration"`
	TimesLeft          struct {
		File     float64 `json:"file"`
		Filament float64 `json:"filament"`
		Layer    float64 `json:"layer"`
	} `json:"timesLeft"`
}

type tpl struct {
	Content string
}
