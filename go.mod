module gitlab.com/Toriniasty/reprap_notify

go 1.13

require (
	github.com/gregdel/pushover v0.0.0-20201104094836-ddbe0c1d3a38
	github.com/spf13/viper v1.7.1
	github.com/xconstruct/go-pushbullet v0.0.0-20171206132031-67759df45fbb
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
