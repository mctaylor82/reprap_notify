package main

import (
	"log"
	"os/exec"
	"runtime"

	"github.com/spf13/viper"
)

func runCommand(status rrStatusType) {
	if len(viper.GetStringSlice(`actions.when.`+returnStatus(status.Status))) > 0 {
		for _, cmdStr := range viper.GetStringSlice(`actions.when.` + returnStatus(status.Status)) {
			go func(cmdStr string) {
				log.Printf(`Running: %v`, cmdStr)
				var cmd []byte
				var err error

				if runtime.GOOS == "windows" {
					cmd, err = exec.Command("cmd", "/c", cmdStr).Output()
				} else {
					cmd, err = exec.Command("sh", "-c", cmdStr).Output()
				}

				if err != nil {
					log.Printf("There was an error while we were trying to execute command: %v", err)
				}

				log.Printf("Command output: %v", string(cmd))
			}(cmdStr)
		}

	}
}
